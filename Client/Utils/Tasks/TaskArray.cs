using System;
using System.Threading.Tasks;

namespace Client.Utils.Tasks;

public class TaskArray<T>
{
    private Task<T>[] _source;

    public TaskArray(int length)
    {
        _source = new Task<T>[length];
    }

    public Task<T> this[int index]
    {
        get => _source[index];
        set => _source[index] = value;
    }

    public Task[] ContinueWith(Action<Task<T>> continuation)
    {
        var contTasks = new Task[_source.Length];
        for (int i = 0; i < _source.Length; ++i)
        {
            contTasks[i] = _source[i].ContinueWith(continuation);
        }
        return contTasks;
    }
}