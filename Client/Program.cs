﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

using Client.Options;
using Client.Extentions;
using Client.Services.Core;
using Client.Services.CliUtils;
using Client.Services.EntryPoints;
using Client.Services.EndpointHelping;
using Client.Services.HttpRequestHelping;


var builder = Host.CreateDefaultBuilder(args);

builder.ConfigureServices(services => {
    services
        .AddOptions<AfterRequestFail>()
        .BindConfiguration(nameof(AfterRequestFail))
        .ValidateDataAnnotations();
    services
        .AddOptions<ServiceAddresses>()
        .BindConfiguration(nameof(ServiceAddresses));
    services
        .AddOptions<Endpoints>()
        .BindConfiguration(nameof(Endpoints));
        
    services.AddHttpClient();
    services.AddAsyncEntryPoint<MainService>();
    services.AddRequestHelper<RequestRepeater>();
    services.AddFilesHandlersProvider<FilesHandlersProvider>();
    services.AddFileHandler<SingleFileHandler>();
    services.AddBinaryAsker<OneKeyAsker>();
    services.AddInput<RepeatableValidatedInputProvider>();
    services.AddEndpointProvider<RequestEndpointProvider>();
});

builder.ConfigureLogging((loggingBuilder) => loggingBuilder.ClearProviders());

var app = builder.BuildEntryPoint();

await app.RunAsync();