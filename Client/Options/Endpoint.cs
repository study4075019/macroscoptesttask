using System.Net.Http;
using System.Collections.Generic;

namespace Client.Options;

public class Endpoint
{
    public HttpMethod Method { get; set; }
    public string ServiceAddressKey { get; set; }
    public string Uri { get; set; }
    public Dictionary<string, object> QueryParameters { get; set; }
}