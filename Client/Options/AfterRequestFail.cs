using System.ComponentModel.DataAnnotations;

namespace Client.Options;

public class AfterRequestFail
{
    [Range(0, int.MaxValue)]
    public int RepeatsCount { get; set; } = 1;

    [Range(0, int.MaxValue)]
    public int AfterDelay { get; set; } = 5000;
}