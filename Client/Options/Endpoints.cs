using System.Net.Http;
using System.Collections.Generic;

namespace Client.Options;

public class Endpoints
{
    public Dictionary<string, Endpoint> KeyValues { get; set; } = new() {
        ["IsPalindrome"] = new Endpoint()
        {
            Method = HttpMethod.Post,
            ServiceAddressKey = "PalindromeService1",
            Uri = "api/is-palindrome",
            QueryParameters = new Dictionary<string, object> {
                ["lettersOnly"] = false
            }
        }
    };
}