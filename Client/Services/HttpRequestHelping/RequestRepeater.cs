using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.Extensions.Options;

using Client.Options;

namespace Client.Services.HttpRequestHelping;

public class RequestRepeater : IHttpRequestHelper
{
    private readonly AfterRequestFail _options;

    public RequestRepeater(IOptions<AfterRequestFail> options) => _options = options.Value;

    public async Task<HttpResponseMessage> RequestAsync(
        HttpContent? requestBody, 
        string? uri, 
        HttpClient client, 
        HttpMethod method)
    {
        var request = new HttpRequestMessage(method, uri)
        {
            Content = requestBody
        };
        var response = await client.SendAsync(request);
        for (int i = 0; i < _options.RepeatsCount && !response.IsSuccessStatusCode; ++i)
        {
            await Task.Delay(_options.AfterDelay);
            response = await client.PostAsync((string?)null, requestBody);
        }
        return response;
    }
}