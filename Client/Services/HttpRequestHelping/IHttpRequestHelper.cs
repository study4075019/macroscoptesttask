using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Client.Services.HttpRequestHelping;

public interface IHttpRequestHelper
{
    Task<HttpResponseMessage> RequestAsync(
        HttpContent? requestBody, 
        string? uri, 
        HttpClient client, 
        HttpMethod method
    );
}