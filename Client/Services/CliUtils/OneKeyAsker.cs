using System;

namespace Client.Services.CliUtils;

public class OneKeyAsker : IYesOrNoAsker
{
    private readonly IRepeatableValidatedInputProvider _inputProvider;

    public OneKeyAsker(IRepeatableValidatedInputProvider inputProvider) => _inputProvider = inputProvider;

    public bool AskWithRepeat(string message)
    {
#pragma warning disable
        return _inputProvider.RepeatableValidatedInput<bool?>(
            $"{message} [y/n]",
            () => Console.ReadKey().Key switch {
                ConsoleKey.Y => true,
                ConsoleKey.N => false,
                _ => null
            },
            (value) => value is null,
            $"Bad Input. {message} [y/n]"
        ).Value;
#pragma warning restore
    }

    public bool AskOnce(bool defaultValue, string message)
    {
        bool yesOrNo;
        Console.WriteLine($"{message} [y/n]");
        yesOrNo = Console.ReadKey().Key switch
            {
                ConsoleKey.Y => true,
                ConsoleKey.N => false,
                _ => defaultValue
            };
        return yesOrNo;
    }
}