using System;

namespace Client.Services.CliUtils;

public interface IRepeatableValidatedInputProvider
{
    public TOut RepeatableValidatedInput<TOut>(
        string firstMessage, 
        Func<TOut> input, 
        Predicate<TOut> notValid, 
        string? repeatMessage
    );
}