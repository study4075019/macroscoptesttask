namespace Client.Services.CliUtils;

public interface IYesOrNoAsker
{
    bool AskOnce(bool defaultValue, string message);

    bool AskWithRepeat(string message);
}