using System;

namespace Client.Services.CliUtils;

public class RepeatableValidatedInputProvider : IRepeatableValidatedInputProvider
{
    public TOut RepeatableValidatedInput<TOut>(
        string firstMessage, 
        Func<TOut> input, 
        Predicate<TOut> notValid, 
        string? repeatMessage = null)
    {
        if (repeatMessage is null)
        {
            repeatMessage = firstMessage;
        }
        Console.WriteLine(firstMessage);
        TOut val = input();
        while (notValid(val))
        {
            Console.WriteLine(repeatMessage);
            val = input();
        }
        return val;
    }
}