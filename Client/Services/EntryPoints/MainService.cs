using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.Extensions.Options;

using Client.Options;
using Client.Extentions;
using Client.Services.Core;
using Client.Services.CliUtils;
using System.Collections.Concurrent;
using Client.Services.EndpointHelping;

namespace Client.Services.EntryPoints;

public class MainService : IAsyncEntryPoint
{
    private readonly IFilesHandlersProvider _handlersProvider;
    private readonly IHttpClientFactory _factory;
    private readonly IYesOrNoAsker _asker;
    private readonly IRepeatableValidatedInputProvider _inputProvider;
    private readonly RequestEndpoint _endpoint;

    public MainService(
        IFilesHandlersProvider handlersProvider,
        IHttpClientFactory factory,
        IYesOrNoAsker asker,
        IRepeatableValidatedInputProvider inputProvider,
        IRequestEndpointProvider endpointProvider)
    {
        _handlersProvider = handlersProvider;
        _factory = factory;
        _asker = asker;
        _inputProvider = inputProvider;
        _endpoint = endpointProvider.GetEndpoint("IsPalindrome");
    }

    public async Task RunAsync()
    {
        try
        {
            await RunInternal();
        }
        catch (Exception e)
        {
            Console.WriteLine($"\nError...\n{e.Message}");
        }
    }

    private async Task RunInternal()
    {
        var dir = _inputProvider.RepeatableValidatedInput<string>(
            "Input directory path:",
            () => Console.ReadLine(),
            (value) => !Directory.Exists(value),
            "Path does not exists. Try again:"
        );

        var filesToHandle = new List<string>(Directory.GetFiles(dir, "*.txt"));
        bool repeat;
        do
        {
            var removePromises = new ConcurrentBag<Task>();

            repeat = false;

            Console.WriteLine("\nResponses:\n");

            var client = _factory.CreateClient();
            client.BaseAddress = _endpoint.Uri;

            var processingTasks = _handlersProvider.GetFilesHandlers(
                filesToHandle,
                filesToHandle.Count, 
                client,
                _endpoint.Method
            );

            var failStream = new MemoryStream();
            using var failStreamWriter = new StreamWriter(failStream)
            {
                AutoFlush = true
            };

            var continueTasks = processingTasks.ContinueWith(
                finishedTask => {
                    var kv = finishedTask.Result;
                    TextWriter writer;
                    if (bool.TryParse(kv.Value, out bool _))
                    {
                        writer = Console.Out;
                        removePromises.Add(Task.Run(() => filesToHandle.Remove(kv.Key)));
                    }
                    else
                    {
                        writer = failStreamWriter;
                    }
                    var fileName = Path.GetFileName(kv.Key);
                    lock (writer)
                    {
                        writer.WriteLine($"{fileName}: {kv.Value}");
                    }
                }
            );

            await Task.WhenAll(continueTasks);

            if (failStream.Length > 0)
            {
                Console.WriteLine("\nSome files was not handled with the following errors:\n");
                failStream.WriteTo(Console.OpenStandardOutput());
                Console.WriteLine();
                repeat = _asker.AskWithRepeat("Try again?");
                Console.WriteLine();
            }

            await Task.WhenAll(removePromises);
        } while (repeat);
    }
}