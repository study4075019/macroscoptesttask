using System.Threading.Tasks;

namespace Client.Services.EntryPoints;

public interface IAsyncEntryPoint
{
    Task RunAsync();
}