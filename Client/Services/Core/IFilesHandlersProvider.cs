using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

using Client.Utils.Tasks;

namespace Client.Services.Core;

public interface IFilesHandlersProvider
{
    TaskArray<KeyValuePair<string, string?>> GetFilesHandlers(IEnumerable<string> files, int filesCount, HttpClient client, HttpMethod method);
}