using System;
using System.Net.Http;
using System.Collections.Generic;

using Client.Utils.Tasks;

namespace Client.Services.Core;

public class FilesHandlersProvider : IFilesHandlersProvider
{
    private readonly ISingleFileHandler _fileHandler;

    public FilesHandlersProvider(ISingleFileHandler fileHandler) => _fileHandler = fileHandler;

    public TaskArray<KeyValuePair<string, string?>> GetFilesHandlers(IEnumerable<string> files, int filesCount, HttpClient client, HttpMethod method)
    {
        if (filesCount == 0)
        {
            throw new Exception("Nothing to proceed");
        }
        if (filesCount < 0)
        {
            throw new ArgumentException("Cannot be less than zero", nameof(filesCount));
        }
        var tasks = new TaskArray<KeyValuePair<string, string?>>(filesCount);
        int i = 0;
        foreach (var file in files)
        {
            tasks[i] = _fileHandler.HandleFileAsync(file, client, method);
            ++i;
        }
        return tasks;
    }
}