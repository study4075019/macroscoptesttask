using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;

using Client.Services.HttpRequestHelping;
using System;

namespace Client.Services.Core;

public class SingleFileHandler : ISingleFileHandler
{
    private readonly IHttpRequestHelper _requestHelper;

    public SingleFileHandler(IHttpRequestHelper requestHelper) => _requestHelper = requestHelper;

    public async Task<KeyValuePair<string, string?>> HandleFileAsync(string fileName, HttpClient client, HttpMethod method)
    {
        var fileText = await File.ReadAllTextAsync(fileName);
        var requestBody = new StringContent(fileText);
        var response = await _requestHelper.RequestAsync(requestBody, null, client, method);
        string? responseBody = null;
        try
        {
            var problem = await response.Content.ReadFromJsonAsync<ProblemDetails>();
            responseBody = $"{problem?.Title} {problem?.Detail}";
        }
        catch
        {
            responseBody = await response.Content.ReadAsStringAsync();
        }
        return new KeyValuePair<string, string?>(fileName, responseBody);
    }
}