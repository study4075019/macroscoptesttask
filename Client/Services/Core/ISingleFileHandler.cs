using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Client.Services.Core;

public interface ISingleFileHandler
{
    Task<KeyValuePair<string, string?>> HandleFileAsync(string fileName, HttpClient client, HttpMethod method);
}