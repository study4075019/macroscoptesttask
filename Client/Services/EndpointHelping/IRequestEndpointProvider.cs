using Client.Options;

namespace Client.Services.EndpointHelping;

public interface IRequestEndpointProvider
{
    public RequestEndpoint GetEndpoint(string endpointName);
}