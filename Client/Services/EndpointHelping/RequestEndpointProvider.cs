using System;
using System.Linq;

using Microsoft.Extensions.Options;

using Client.Options;

namespace Client.Services.EndpointHelping;

public class RequestEndpointProvider : IRequestEndpointProvider
{
    private ServiceAddresses _addresses;
    private Endpoints _endpoints;

    public RequestEndpointProvider(
        IOptions<ServiceAddresses> addresses,
        IOptions<Endpoints> endpoints)
    {
        _addresses = addresses.Value;
        _endpoints = endpoints.Value;
    }
    public RequestEndpoint GetEndpoint(string endpointName)
    {
        var endpoint = _endpoints.KeyValues[endpointName];
        var parametersValues = 
            from kvPair in endpoint.QueryParameters
            select $"{kvPair.Key}={kvPair.Value}";
        return new RequestEndpoint()
        {
            Method = endpoint.Method,
            Uri = new Uri($"{_addresses.KeyValues[endpoint.ServiceAddressKey]}{endpoint.Uri}?{string.Join('&', parametersValues)}")
        };
    }
}