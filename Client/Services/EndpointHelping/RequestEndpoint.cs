using System;
using System.Net.Http;

namespace Client.Services.EndpointHelping;

public class RequestEndpoint
{
    public HttpMethod Method { get; set; }

    public Uri Uri { get; set; }
}