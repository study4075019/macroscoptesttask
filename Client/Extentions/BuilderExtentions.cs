using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

using Client.Services.EntryPoints;

namespace Client.Extentions;

public static class BuilderExtentions
{
    public static IAsyncEntryPoint BuildEntryPoint(this IHostBuilder builder)
        => builder.Build().Services.GetRequiredService<IAsyncEntryPoint>();
}