using Microsoft.Extensions.DependencyInjection;

using Client.Services.Core;
using Client.Services.CliUtils;
using Client.Services.EntryPoints;
using Client.Services.EndpointHelping;
using Client.Services.HttpRequestHelping;

namespace Client.Extentions;

public static class ServiceCollectionExtentions
{
    public static IServiceCollection AddAsyncEntryPoint<TImplemenation>(this IServiceCollection services)
        where TImplemenation : class, IAsyncEntryPoint
    {
        return services.AddSingleton<IAsyncEntryPoint, TImplemenation>();
    }

    public static IServiceCollection AddRequestHelper<TImplemenation>(this IServiceCollection services)
        where TImplemenation : class, IHttpRequestHelper
    {
        return services.AddTransient<IHttpRequestHelper, TImplemenation>();
    }

    public static IServiceCollection AddFilesHandlersProvider<TImplemenation>(this IServiceCollection services)
        where TImplemenation : class, IFilesHandlersProvider
    {
        return services.AddTransient<IFilesHandlersProvider, TImplemenation>();
    }

    public static IServiceCollection AddFileHandler<TImplemenation>(this IServiceCollection services)
        where TImplemenation : class, ISingleFileHandler
    {
        return services.AddTransient<ISingleFileHandler, TImplemenation>();
    }

    public static IServiceCollection AddBinaryAsker<TImplemenation>(this IServiceCollection services)
        where TImplemenation : class, IYesOrNoAsker
    {
        return services.AddTransient<IYesOrNoAsker, TImplemenation>();
    }

    public static IServiceCollection AddInput<TImplemenation>(this IServiceCollection services)
        where TImplemenation : class, IRepeatableValidatedInputProvider
    {
        return services.AddTransient<IRepeatableValidatedInputProvider, TImplemenation>();
    }

    public static IServiceCollection AddEndpointProvider<TImplemenation>(this IServiceCollection services)
        where TImplemenation : class, IRequestEndpointProvider
    {
        return services.AddTransient<IRequestEndpointProvider, TImplemenation>();
    }
}