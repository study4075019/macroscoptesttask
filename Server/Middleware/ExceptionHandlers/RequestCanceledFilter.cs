using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Server.Middleware.ExceptionHandlers;

public class RequestCanceledFilter : IExceptionFilter
{
    private readonly ILogger<RequestCanceledFilter> _logger;
    public RequestCanceledFilter(ILogger<RequestCanceledFilter> logger) => _logger = logger;
    public void OnException(ExceptionContext context)
    {
        if (context.Exception is OperationCanceledException or TaskCanceledException)
        {
            var httpContext = context.HttpContext;
            context.Result = new StatusCodeResult(StatusCodes.Status499ClientClosedRequest);
            context.ExceptionHandled = true;
            _logger.LogInformation($"Endpoint: {httpContext.Request.Path + httpContext.Request.QueryString}");
        }
    }
}