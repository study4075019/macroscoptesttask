using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Diagnostics;

using Server.Services.PromlemDetailsResponseHelping;

namespace Server.Middleware.ExceptionHandlers;

public class GeneralExceptionHandler : IExceptionHandler
{
    private readonly IProblemDetailsResponseHelper _responseService;

    public GeneralExceptionHandler(IProblemDetailsResponseHelper responseService)
    {
        _responseService = responseService;
    }

    public async ValueTask<bool> TryHandleAsync(HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
    {
        await _responseService.SetResponseAsync(
            httpContext, 
            StatusCodes.Status500InternalServerError,
            "about:blank",
            "",
            "Something went wrong",
            cancellationToken
        );
        return true;
        // not need to log, asp middleware log when UseExceptionHandler<THandler>()
    }
}