using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace Server.Middleware.Formatters;

public class PlainTextFormatter : TextInputFormatter
{
    public PlainTextFormatter()
    {
        SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/plain"));
    }

    protected override bool CanReadType(Type type) => type == typeof(string);

    private async Task<InputFormatterResult> ReadRequestBodyAsyncInternal(
        InputFormatterContext context, 
        Encoding? enc = null)
    {
        var body = context.HttpContext.Request.Body;
        StreamReader reader;
        if (enc is null)
        {
            reader = new StreamReader(body);
        }
        else
        {
            reader = new StreamReader(body, enc);
        }
        var readed = await reader.ReadToEndAsync();
        reader.Dispose();
        var result = await InputFormatterResult.SuccessAsync(readed);
        return result;
    }

    public override async Task<InputFormatterResult> ReadRequestBodyAsync(
        InputFormatterContext context
    ) => await ReadRequestBodyAsyncInternal(context);

    public override async Task<InputFormatterResult> ReadRequestBodyAsync(
        InputFormatterContext context, 
        Encoding enc
    ) => await ReadRequestBodyAsyncInternal(context, enc);
}