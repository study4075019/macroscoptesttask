using System;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.RateLimiting;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.RateLimiting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

using Server.Services.PromlemDetailsResponseHelping;

namespace Server.Middleware.Limiters;

public class ConcurrencyLimiterPolicy : IRateLimiterPolicy<string>
{
    private readonly ConcurrencyRequestsLimits _options;
    private readonly ILogger<ConcurrencyLimiterPolicy> _logger;  
    private readonly IProblemDetailsResponseHelper _responseService;
    private readonly string rejectDetails;

    public ConcurrencyLimiterPolicy(
        ILogger<ConcurrencyLimiterPolicy> logger,
        IOptions<ConcurrencyRequestsLimits> options,
        IProblemDetailsResponseHelper responseService)
    {
        _options = options.Value;
        if (_options.PermitLimit <= 0)
        {
            throw new ArgumentException(
                $"{nameof(_options.PermitLimit)} value must be greater than 0", 
                nameof(options)
            );
        }
        _logger = logger;
        _responseService = responseService;
        rejectDetails = "Request was rejected because the server had already reached the limit of handled requests.";
    }

    public Func<OnRejectedContext, CancellationToken, ValueTask>? OnRejected 
    => async (context, cToken) => {
        var httpContext = context.HttpContext;
        var writePromise = _responseService.SetResponseAsync(
            httpContext, 
            StatusCodes.Status429TooManyRequests,
            "about:blank",
            "Please, try again later.",
            rejectDetails,
            cToken
        );
        _logger.LogWarning(
            $"{rejectDetails}\n" +
            $"\tPermit limit: {_options.PermitLimit}\n" +
            $"\tEndpoint: {httpContext.Request.Path}{httpContext.Request.QueryString}"
        );
        await writePromise;
    };

    public RateLimitPartition<string> GetPartition(HttpContext context) 
    => RateLimitPartition.GetConcurrencyLimiter(
        nameof(ConcurrencyLimiterPolicy),
        (notUsedKey) => new ConcurrencyLimiterOptions() {
            PermitLimit = _options.PermitLimit,
            QueueLimit = _options.QueueLimit,
            QueueProcessingOrder = QueueProcessingOrder.OldestFirst,
        }
    );
}