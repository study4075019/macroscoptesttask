using System;
using System.ComponentModel.DataAnnotations;

namespace Server.Middleware.Limiters;

/// <summary>
/// Options для конфигурации политики промежуточного ПО,
/// обеспечивающего максимальное количество одновременно
/// обрабатываемых запросов сервером 
/// </summary>
public class ConcurrencyRequestsLimits
{
    private int _permitLimit;
    private int _queueLimit;

    [Range(1, int.MaxValue)]
    public int PermitLimit 
    { 
        get => _permitLimit; 
        set
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }
            _permitLimit = value;
        }
    }

    [Range(0, int.MaxValue)]        
    public int QueueLimit 
    { 
        get => _queueLimit; 
        set
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }
            _queueLimit = value;
        }
    }
}