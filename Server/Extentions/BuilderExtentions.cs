using System.Collections.Generic;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Server.Middleware.Limiters;

namespace Server.Extentions;

public static class BuilderExtentions
{
    public static void AddConcurrencyOptions(
        this IHostApplicationBuilder builder, 
        string[] args,
        string permitLimitCmdArgName)
    {
        builder.Configuration.AddCommandLine(args, new Dictionary<string, string> {
            { 
                $"--{permitLimitCmdArgName}", 
                $"{nameof(ConcurrencyRequestsLimits)}:{nameof(ConcurrencyRequestsLimits.PermitLimit)}" 
            },
        });

        builder.Services
            .AddOptions<ConcurrencyRequestsLimits>()
            .BindConfiguration(nameof(ConcurrencyRequestsLimits))
            .ValidateDataAnnotations();
    }
}