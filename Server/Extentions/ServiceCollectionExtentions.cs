using Microsoft.Extensions.DependencyInjection;

using Server.Services.Palindrome;
using Server.Services.PromlemDetailsResponseHelping;

namespace Server.Extentions;

public static class ServiceCollectionExtentions
{
    public static IServiceCollection AddProblemDetailsResponseHelper<TImplementation>(this IServiceCollection services)
        where TImplementation : class, IProblemDetailsResponseHelper
    {
        return services.AddSingleton<IProblemDetailsResponseHelper, TImplementation>();
    }

    public static IServiceCollection AddPalindromeService<TImplementation>(this IServiceCollection services)
        where TImplementation : class, IPalindromeService
    {
        return services.AddScoped<IPalindromeService, TImplementation>();
    }
}
