using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

using Server.Extentions;
using Server.Middleware.Limiters;
using Server.Middleware.Formatters;
using Server.Middleware.ExceptionHandlers;
using Server.Services.Palindrome;
using Server.Services.PromlemDetailsResponseHelping;


var builder = WebApplication.CreateBuilder(args);

///  Здесь можно назначить имя аргумента командной строки, 
///  значение которого равно максимальному количеству обрабатываемых запросов
builder.AddConcurrencyOptions(args, "permit-limit");

builder.Services.AddControllers(options => 
{
    options.InputFormatters.Add(new PlainTextFormatter());
    options.Filters.Add<RequestCanceledFilter>();
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddRateLimiter(options => 
{
    options.AddPolicy<string, ConcurrencyLimiterPolicy>(nameof(ConcurrencyLimiterPolicy));
});


if (builder.Environment.IsProduction())
{
    // add another handler before this handler
    builder.Services.AddExceptionHandler<GeneralExceptionHandler>();
    builder.Services.AddProblemDetailsResponseHelper<ProblemDetailsResponseHelper>();
}
// else will be used DeveloperExceptionPages with it's own logging

builder.Services.AddPalindromeService<LongCalculatingPalindromeService>();

var app = builder.Build();

app.UseRateLimiter();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
else if (app.Environment.IsProduction())
{
    app.UseExceptionHandler(_ => { });
}

app.UseHttpsRedirection();

app.MapControllers();

app.Run();