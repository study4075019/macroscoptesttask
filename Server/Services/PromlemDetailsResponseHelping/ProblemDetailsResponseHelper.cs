using System.Net;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Server.Services.PromlemDetailsResponseHelping;

public class ProblemDetailsResponseHelper : IProblemDetailsResponseHelper
{
    private readonly Regex _oneWordRegex = new Regex("[A-Z][^A-Z]*");

    public async Task SetResponseAsync(
        HttpContext context, 
        int statusCode, 
        string type, 
        string title,
        string detail,
        CancellationToken token)
    {
        context.Response.StatusCode = statusCode;
        context.Response.ContentType = "application/json";
        var str = ((HttpStatusCode)context.Response.StatusCode).ToString();
        var matches = _oneWordRegex.Matches(str);
        var words = matches.Select(m => m.Value);
        var allTitle = $"{string.Join(" ", words)}. {title}";
        await context.Response.WriteAsJsonAsync(
            new ProblemDetails
            {
                Status = context.Response.StatusCode,
                Type = type,
                Title = allTitle,
                Detail = detail,
                Instance = context.Request.Host + context.Request.Path + context.Request.QueryString
            },
            token
        );
    }
}