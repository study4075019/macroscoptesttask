using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

namespace Server.Services.PromlemDetailsResponseHelping;

public interface IProblemDetailsResponseHelper
{
    public Task SetResponseAsync(
        HttpContext context,  
        int statusCode, 
        string type, 
        string title,
        string detail,
        CancellationToken token
    );
}