using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Server.Services.Palindrome;

public class LongCalculatingPalindromeService : IPalindromeService
{
    public async Task<bool> IsPalindromeAsync(string source, bool lettersOnly, CancellationToken cToken)
    {
        var promise = Task.Delay(TimeSpan.FromSeconds(5)); // иммитация долгой работы
        
        if (lettersOnly)
        {
            var letters = source
                .AsParallel()
                .AsOrdered()
                .Where(symbol => char.IsLetter(symbol));

            cToken.ThrowIfCancellationRequested();

            source = new string(letters.ToArray());
        }
        
        bool isPalindrome = true;
        int lastIndex = source.Length - 1;

        for (int i = 0; i < source.Length / 2 && isPalindrome; ++i)
        {
            cToken.ThrowIfCancellationRequested();
            isPalindrome = source[i] == source[lastIndex - i];
        }

        while (promise.Status != TaskStatus.RanToCompletion)
        {
            cToken.ThrowIfCancellationRequested();
            await Task.Delay(3000);
        } 

        await promise; // ждем, если 5 секунд еще не прошли
        return isPalindrome;
    }
}