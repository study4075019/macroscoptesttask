using System.Threading;
using System.Threading.Tasks;

namespace Server.Services.Palindrome;

public interface IPalindromeService
{
    Task<bool> IsPalindromeAsync(string source, bool lettersOnly, CancellationToken cToken);
}