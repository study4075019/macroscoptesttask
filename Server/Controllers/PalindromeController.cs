using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.RateLimiting;
using Microsoft.Extensions.Logging;

using Server.Middleware.Limiters;
using Server.Services.Palindrome;

namespace Server.Controllers;

[ApiController]
[Route("api")]
public class PalindromeController : ControllerBase
{
    private readonly IPalindromeService _palindromeService;

    public PalindromeController(IPalindromeService palindromeService) 
        => _palindromeService = palindromeService;

    [HttpPost("is-palindrome")]
    [EnableRateLimiting(nameof(ConcurrencyLimiterPolicy))]
    [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(void), StatusCodes.Status499ClientClosedRequest)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> IsPalindromeAsync(
        [FromQuery] bool lettersOnly, 
        [FromBody] string source, 
        CancellationToken cToken
    ) => Ok(await _palindromeService.IsPalindromeAsync(source, lettersOnly, cToken));
}